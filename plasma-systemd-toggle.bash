#!/usr/bin/env bash

FILE="$HOME/.config/startkderc"

plasmaLogout () {
    if [ -x "$(command -v qdbus)" ]
    then
        qdbus org.kde.ksmserver /KSMServer logout 1 3 3
    elif [ -x "$(command -v qdbus-qt5)" ]
    then
        qdbus-qt5 org.kde.ksmserver /KSMServer logout 1 3 3
    elif [ -x "$(command -v qdbus6)" ]
    then
        qdbus6 org.kde.ksmserver /KSMServer logout 1 3 3
    else
        kdialog --error "You don't have any qdbus utility!\nIt was not possible to log out.\nPlease install it from your Linux distribution."
        exit 1
    fi
}

if [[ -e $FILE ]] ; then
    rm $FILE
    kdialog --yesno "The systemd startup has been DISABLED. Would you like to log out?" --title "Systemd Startup"
    DISABLE_LOGOUT=$?
    if [ "$DISABLE_LOGOUT" == 0 ]
    then
        plasmaLogout
    fi
else
    kwriteconfig"${KDE_SESSION_VERSION}" --file startkderc --group General --key systemdBoot true
    kdialog --yesno "The systemd startup has been ENABLED. Would you like to log out?" --title "Systemd Startup"
    ENABLE_LOGOUT=$?
    if [ "$ENABLE_LOGOUT" == 0 ]
    then
        plasmaLogout
    fi
fi
