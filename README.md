# kdialogscripts

This is a repository to store useful Bash scripts with a KDialog GUI.

Read the scripts before running. In fact, NEVER RUN ANY SCRIPTS YOU GRAB FROM THE INTERNET WITHOUT READING THEM!

These scripts attempt to be smart and detect if you use qdbus, qdbus-qt5 or qdbus6.

If you get an error, report the issue over https://invent.kde.org/thiagosueto/kdialogscripts/-/issues.

The .bash extension is there because gitlab changes script icons based on their extension, and for the install script to install only the right files.

![](overview-as-meta.webp)

## Available scripts

* meta-krunner: this enables or disables KRunner being triggered with Meta. You don't need to disable any keyboard shortcuts to use this, as KWin's shortcuts have higher priority than other shortcuts. Disabling use of Meta with KRunner for this script will make Meta open whatever other functionality was originally assigned to it.

* meta-overview: this toggles the Overview and set it to trigger with Meta. Disabling the shortcut with this script will also disable the effect, you'll need to rerun this script or reenable the desktop effect in the settings.

* plasma-systemd-toggle: enables or disables the Plasma systemd startup. It requires a logout for changes to be saved, in which case the user will be prompted to do so.
